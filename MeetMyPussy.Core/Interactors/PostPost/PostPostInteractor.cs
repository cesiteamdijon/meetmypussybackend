﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.PostPost
{
    public class PostPostInteractor
    {
        /// <summary>
        /// Dépot des Profils
        /// </summary>
        private IUserRepository _repository;


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PostPostOutput Output;

        public PostPostInteractor(IUserRepository repository)
        {
            _repository = repository;
        }

        public void Execute(PostPostInput input)
        {
            Output = new PostPostOutput()
            {
                Wall = _repository.CreatePost(new PostEntity()
                {
                    Owner = input.User,
                    Date = DateTime.Now,
                    Content = input.Content
                }, input.WallOwnerId)
            };


        }
    }
}
