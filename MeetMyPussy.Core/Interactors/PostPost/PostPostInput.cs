﻿using MeetMyPussy.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.PostPost
{
    public class PostPostInput
    {
        /// <summary>
        /// Utilisateur qui créé le post
        /// </summary>
        public UserEntity User { get; set; }

        /// <summary>
        /// Contenu du post
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Id de l'utilisateur qui possède le mur sur lequel le post est créé
        /// </summary>
        public string WallOwnerId { get; set; }
    }
}
