﻿using MeetMyPussy.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.GetUser
{
    public class GetUserInteractor
    {
        /// <summary>
        /// Dépot des utilisateurs
        /// </summary>
        private IUserRepository _repository;

        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public GetUserOutput Output;

        /// <summary>
        /// Fonctionnalité : Obtenir les données de l'utilisateur identifié
        /// </summary>
        /// <param name="repository"></param>
        public GetUserInteractor(IUserRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(GetUserInput input)
        {
            Output = new GetUserOutput()
            {
                User = _repository.Get(input.Id)
            };
        }
    }
}
