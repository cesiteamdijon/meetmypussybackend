﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Repositories;
using MeetMyPussy.Core.Services;

namespace MeetMyPussy.Core.Interactors
{
    /// <summary>
    /// Créé un user.
    /// </summary>
    public class PostSingleUserInteractor
    {

        /// <summary>
        /// Dépot des utilisateurs
        /// </summary>
        private IUserRepository _repository { get; set; }


        /// <summary>
        /// Service d'authentification
        /// </summary>
        private IAuthService _authService { get; set; }


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PostSingleUserOutput Output { get; set; }


        /// <summary>
        /// Fonctionnalité : Creer un utilisateur
        /// </summary>
        /// <param name="repository"></param>
        public PostSingleUserInteractor(
            IUserRepository repository,
            IAuthService authService)
        {
            _repository = repository;
            _authService = authService;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(PostSingleUserInput input)
        {
            UserEntity user = _repository.Create(new UserEntity
            {
                Email = input.Email
            },
            _authService.HashPassword(input.Password));

            Output = new PostSingleUserOutput()
            {
                User = user
            };
        }
    }
}
