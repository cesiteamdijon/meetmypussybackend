﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors
{
    public class PostSingleUserOutput
    {

        /// <summary>
        /// Utilisateur créé
        /// </summary>
        public UserEntity User { get; set; }
    }
}
