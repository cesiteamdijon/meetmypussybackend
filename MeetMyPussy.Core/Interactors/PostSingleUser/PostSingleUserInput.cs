﻿namespace MeetMyPussy.Core.Interactors
{
    public class PostSingleUserInput
    {

        /// <summary>
        /// Email de l'utilisateur à créer
        /// </summary>
        public string Email { get; set; }


        /// <summary>
        /// Mot de passe de l'utilisateur à créer
        /// </summary>
        public string Password { get; set; }
    }
}
