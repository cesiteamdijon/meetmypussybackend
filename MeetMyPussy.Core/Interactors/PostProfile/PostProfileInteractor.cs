﻿using MeetMyPussy.Core.Repositories;

namespace MeetMyPussy.Core.Interactors.PostProfile
{
    /// <summary>
    /// Créé un profil. L'id du user associé doit être précisé dans Input.Profile.IdUser
    /// </summary>
    public class PostProfileInteractor
    {

        /// <summary>
        /// Dépot des Profils
        /// </summary>
        private IProfileRepository _repository;


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PostProfileOutput Output;


        /// <summary>
        /// Fonctionnalité : Creer un profil
        /// </summary>
        /// <param name="repository"></param>
        public PostProfileInteractor(IProfileRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(PostProfileInput input)
        {
            Output = new PostProfileOutput()
            {
                Profile = _repository.AttachProfileToUser(input.UserId, input.Profile)
            };
        }
    }
}
