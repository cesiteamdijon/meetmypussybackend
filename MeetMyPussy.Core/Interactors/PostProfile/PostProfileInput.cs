﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PostProfile
{
    public class PostProfileInput
    {

        /// <summary>
        /// Identifiant de l'utilisateur
        /// </summary>
        public string UserId { get; set; }


        /// <summary>
        /// Profil à créer
        /// </summary>
        public ProfileEntity Profile { get; set; }
    }
}
