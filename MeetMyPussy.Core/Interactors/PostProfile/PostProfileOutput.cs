﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PostProfile
{
    public class PostProfileOutput
    {

        /// <summary>
        /// Identifiant de l'utilisateur
        /// </summary>
        public string UserId { get; set; }


        /// <summary>
        /// Profil créé
        /// </summary>
        public ProfileEntity Profile {get;set;}
    }
}
