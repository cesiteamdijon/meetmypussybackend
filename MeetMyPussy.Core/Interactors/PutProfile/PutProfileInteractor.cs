﻿using MeetMyPussy.Core.Repositories;

namespace MeetMyPussy.Core.Interactors.PutProfile
{
    /// <summary>
    /// Met à jour le profil d'un User. L'Id user doit être précisé dans Input.Profile.IdUser
    /// </summary>
    public class PutProfileInteractor
    {
        /// <summary>
        /// Dépot des profils
        /// </summary>
        private IProfileRepository _repository;


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PutProfileOutput Output;


        /// <summary>
        /// Fonctionnalité : Modifier un profil
        /// </summary>
        /// <param name="repository"></param>
        public PutProfileInteractor(IProfileRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="Input"></param>
        public void Execute(PutProfileInput Input)
        {
            Output = new PutProfileOutput()
            {
                Profile = _repository.Update(Input.IdUser, Input.Profile)
            };
        }
    }
}
