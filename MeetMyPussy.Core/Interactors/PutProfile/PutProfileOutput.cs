﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PutProfile
{
    public class PutProfileOutput
    {

        /// <summary>
        /// Profil modifié
        /// </summary>
        public ProfileEntity Profile { get; set; }
    }
}
