﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PutProfile
{
    public class PutProfileInput
    {
        /// <summary>
        /// Identifiant de l'utilisateur
        /// </summary>
        public string IdUser { get; set; }

        /// <summary>
        /// Profil à modifier
        /// </summary>
        public ProfileEntity Profile { get; set; }
    }
}
