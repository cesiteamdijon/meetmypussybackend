﻿using MeetMyPussy.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.GetProfilesByName
{
    public class GetProfilesByNameOutput
    {
        /// <summary>
        /// Profils des utilisateurs trouvés
        /// </summary>
        public List<UserEntity> Users { get; set; }
    }
}
