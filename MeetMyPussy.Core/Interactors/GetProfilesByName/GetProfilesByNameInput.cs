﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.GetProfilesByName
{
    public class GetProfilesByNameInput
    {
        /// <summary>
        /// Nom du/des utilisateur(s) recherché(s)
        /// </summary>
        public string Name { get; set; }
    }
}
