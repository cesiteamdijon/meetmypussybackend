﻿using MeetMyPussy.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Interactors.GetProfilesByName
{
    public class GetProfilesByNameInteractor
    {
        /// <summary>
        /// Dépot des profils
        /// </summary>
        private IUserRepository _repository;

        public GetProfilesByNameOutput Output;

        /// <summary>
        /// Fonctionnalité : Obtenir le(s) profil(s) du/des utilisateur(s) recherché(s)
        /// </summary>
        /// <param name="repository"></param>
        public GetProfilesByNameInteractor(IUserRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(GetProfilesByNameInput input)
        {
            Output = new GetProfilesByNameOutput()
            {
                Users = _repository.GetByName(input.Name)
            };
        }
    }
}
