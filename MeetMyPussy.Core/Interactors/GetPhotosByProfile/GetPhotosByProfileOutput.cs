﻿using MeetMyPussy.Core.Entities;
using System.Collections.Generic;

namespace MeetMyPussy.Core.Interactors.GetPhotosByProfile
{
    public class GetPhotosByProfileOutput
    {
        /// <summary>
        /// Liste des photos du profil
        /// </summary>
        public List<PhotoEntity> Photos { get; set; }
    }
}
