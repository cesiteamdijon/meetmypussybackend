﻿using MeetMyPussy.Core.Repositories;

namespace MeetMyPussy.Core.Interactors.GetPhotosByProfile
{
    public class GetPhotosByProfileInteractor
    {

        /// <summary>
        /// Dépot des photos
        /// </summary>
        private IPhotoRepository _repository;

        
        /// <summary>
        /// Dépot des profils
        /// </summary>
        private IProfileRepository _repositoryProfile;


        /// <summary>
        /// Résultat de l'interactor
        /// </summary>
        public GetPhotosByProfileOutput Output;


        /// <summary>
        /// Initialise l'interactor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryProfile"></param>
        public GetPhotosByProfileInteractor(IPhotoRepository repository, IProfileRepository repositoryProfile)
        {
            _repository = repository;
            _repositoryProfile = repositoryProfile;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(GetPhotosByProfileInput input)
        {

            Output = new GetPhotosByProfileOutput()
            {
                //Photos = _repository.GetAllByProfile(_repositoryProfile.GetByUser(input.User.Id).Id)
            };
        }
    }
}
