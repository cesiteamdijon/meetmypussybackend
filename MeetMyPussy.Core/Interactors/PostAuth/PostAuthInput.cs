﻿namespace MeetMyPussy.Core.Interactors.PostAuth
{
    public class PostAuthInput
    {

        /// <summary>
        /// Email de l'utilisateur
        /// </summary>
        public string Email { get; set; }


        /// <summary>
        /// Mot de passe de l'utilisateur
        /// </summary>
        public string Password { get; set; }
    }
}
