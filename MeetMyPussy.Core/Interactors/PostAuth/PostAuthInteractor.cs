﻿using MeetMyPussy.Core.Repositories;
using MeetMyPussy.Core.Services;

namespace MeetMyPussy.Core.Interactors.PostAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class PostAuthInteractor
    {

        /// <summary>
        /// Dépot des utilisateurs
        /// </summary>
        private IUserRepository _repository;


        /// <summary>
        /// Service d'authentification
        /// </summary>
        private IAuthService _authService { get; set; }


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PostAuthOutput Output;


        /// <summary>
        /// Fonctionnalité : Connecter un utilisateur
        /// </summary>
        /// <param name="repository"></param>
        public PostAuthInteractor(IUserRepository repository, IAuthService authService)
        {
            _repository = repository;
            _authService = authService;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(PostAuthInput input)
        {
            Output = new PostAuthOutput()
            {
                User = _repository.Get(input.Email, _authService.HashPassword(input.Password))
            };
        }
    }
}
