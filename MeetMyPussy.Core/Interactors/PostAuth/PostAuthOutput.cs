﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PostAuth
{
    public class PostAuthOutput
    {

        /// <summary>
        /// Utilisateur authentifié
        /// </summary>
        public UserEntity User {get;set;}
    }
}
