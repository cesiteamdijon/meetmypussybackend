﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PostPhoto
{
    public class PostPhotoInput
    {

        /// <summary>
        /// Photo à creer
        /// </summary>
        public PhotoEntity Photo { get; set; }


        /// <summary>
        /// Utilisateur propriétaire de la photo
        /// </summary>
        public UserEntity User { get; set; }
    }
}
