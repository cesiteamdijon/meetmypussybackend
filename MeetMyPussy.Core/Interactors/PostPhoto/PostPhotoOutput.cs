﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.PostPhoto
{
    public class PostPhotoOutput
    {
        /// <summary>
        /// Photo crée
        /// </summary>
        public PhotoEntity Photo { get; set; }
    }
}
