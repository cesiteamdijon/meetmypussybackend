﻿using MeetMyPussy.Core.Repositories;

namespace MeetMyPussy.Core.Interactors.PostPhoto
{
    public class PostPhotoInteractor
    {

        /// <summary>
        /// Dépot des photos
        /// </summary>
        private IPhotoRepository _repository;


        /// <summary>
        /// Dépot des profils
        /// </summary>
        private IProfileRepository _repositoryProfile;


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public PostPhotoOutput Output;


        /// <summary>
        /// Fonctionnalité : Ajouter une photos à un profil
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryProfile"></param>
        public PostPhotoInteractor(IPhotoRepository repository, IProfileRepository repositoryProfile)
        {
            _repository = repository;
            _repositoryProfile = repositoryProfile;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(PostPhotoInput input)
        {
            //input.Photo.IdProfile = _repositoryProfile.GetByUser(input.User.Id).Id;
            Output = new PostPhotoOutput()
            {
                Photo = _repository.Create(input.Photo)
            };
        }
    }
}
