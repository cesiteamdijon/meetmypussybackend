﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.GetProfile
{
    public class GetProfileByUserInput
    {
        /// <summary>
        /// Utilisateur du profil
        /// </summary>
        public UserEntity User { get; set; }
    }
}
