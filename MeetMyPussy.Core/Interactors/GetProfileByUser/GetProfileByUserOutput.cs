﻿using MeetMyPussy.Core.Entities;

namespace MeetMyPussy.Core.Interactors.GetProfile
{
    public class GetProfileByUserOutput
    {

        /// <summary>
        /// Profil de l'utilisateur
        /// </summary>
        public ProfileEntity Profile {get;set;}
    }
}
