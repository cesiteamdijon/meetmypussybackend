﻿using MeetMyPussy.Core.Repositories;

namespace MeetMyPussy.Core.Interactors.GetProfile
{
    public class GetProfileByUserInteractor
    {

        /// <summary>
        /// Dépot des profils
        /// </summary>
        private IProfileRepository _repository;


        /// <summary>
        /// Résultat de la fonctionnalité
        /// </summary>
        public GetProfileByUserOutput Output;


        /// <summary>
        /// Fonctionnalité : Obtenir le profil d'un utilisateur
        /// </summary>
        /// <param name="repository"></param>
        public GetProfileByUserInteractor(IProfileRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Execute la fonctionnalité
        /// </summary>
        /// <param name="input"></param>
        public void Execute(GetProfileByUserInput input)
        {
            Output = new GetProfileByUserOutput()
            {
                Profile = _repository.GetByUser(input.User.Id)
            };
        }
    }
}
