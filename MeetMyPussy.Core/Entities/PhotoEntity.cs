﻿namespace MeetMyPussy.Core.Entities
{
    public class PhotoEntity
    {

        /// <summary>
        /// Identifiant de la photo
        /// </summary>
        public string Id { get; set; }

    
        /// <summary>
        /// Blob contenant la photo
        /// </summary>
        public byte[] Content { get; set; }


        /// <summary>
        /// Type et extension de la photo
        /// </summary>
        public string MimeType { get; set; }
    }
}
