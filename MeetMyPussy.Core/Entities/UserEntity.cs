﻿namespace MeetMyPussy.Core.Entities
{
    public class UserEntity
    {

        /// <summary>
        /// Identifiant de l'utilisateur
        /// </summary>
        public string Id { get; set; }
        

        /// <summary>
        /// Adresse email de l'utilisateur
        /// </summary>
        public string Email { get; set; }


        /// <summary>
        /// Profil de l'utilisateur
        /// </summary>
        public ProfileEntity Profile { get; set; }

        /// <summary>
        /// Mur de l'utilisateur
        /// </summary>
        public WallEntity Wall { get; set; }
    }
}
