﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Entities
{
    public class PostEntity 
    {
        /// <summary>
        /// Date d'envoi du message
        /// </summary>
        public DateTime Date { get; set; }


        /// <summary>
        /// Utilisateur propriétaire du message
        /// </summary>
        public UserEntity Owner { get; set; }


        /// <summary>
        /// Contenu du message
        /// </summary>
        public string Content { get; set; }
    }
}
