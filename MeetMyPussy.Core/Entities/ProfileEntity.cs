﻿using System.Collections.Generic;

namespace MeetMyPussy.Core.Entities
{
    public class ProfileEntity
    {


        /// <summary>
        /// Nom du profil
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Race du profil
        /// </summary>
        public string Race { get; set; }


        /// <summary>
        /// Genre du profil
        /// </summary>
        public string Gender { get; set; }


        /// <summary>
        /// Pelage du prof
        /// </summary>
        public string Coat { get; set; }


        /// <summary>
        /// Ville du profil
        /// </summary>
        public string City { get; set; }


        /// <summary>
        /// Chaine d'identification
        /// </summary>
        public string Identification { get; set; }


        /// <summary>
        /// Description du profil
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// Liste des photos du profil
        /// </summary>
        public List<PhotoEntity> Photos { get; set; }


        /// <summary>
        /// Photos de profil
        /// </summary>
        public PhotoEntity MainPhoto { get; set; }


        /// <summary>
        /// Photos de la bannière
        /// </summary>
        public PhotoEntity BannerPhoto { get; set; }


        /// <summary>
        /// Liste des amis du profil
        /// </summary>
        public List<UserEntity> Friends { get; set; }
    }
}
