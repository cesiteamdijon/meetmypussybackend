﻿using MeetMyPussy.Core.Entities;
using System.Collections.Generic;

namespace MeetMyPussy.Core.Repositories
{
    public interface IPhotoRepository : IRepository<PhotoEntity>
    {
        List<PhotoEntity> GetAllByUser(string idUser);
    }
}
