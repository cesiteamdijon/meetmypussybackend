﻿using MeetMyPussy.Core.Entities;
using System.Collections.Generic;

namespace MeetMyPussy.Core.Repositories
{
    public interface IProfileRepository : IRepository<ProfileEntity>
    {
        ProfileEntity GetByUser(string id);


        ProfileEntity AttachProfileToUser(string userId, ProfileEntity profileToAttach);
        
    }
}
