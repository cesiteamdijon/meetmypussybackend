﻿using MeetMyPussy.Core.Entities;
using System.Collections.Generic;

namespace MeetMyPussy.Core.Repositories
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        UserEntity Get(string login, string passwordHash);


        UserEntity Create(UserEntity item, string passwordHash);

        WallEntity CreatePost(PostEntity post, string userId);
        UserEntity Update(string id, UserEntity itemIn, string passwordHash);
        List<UserEntity> GetByName(string name);
    }
}
