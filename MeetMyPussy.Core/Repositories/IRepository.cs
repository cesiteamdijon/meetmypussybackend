﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMyPussy.Core.Repositories
{
    public interface IRepository<T>
    {
        List<T> Get();
        T Get(string id);
        T Create(T item);
        T Update(string id, T itemIn);
    }
}
