﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Repositories;
using MeetMyPussy.DataAccess.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeetMyPussy.DataAccess.Repositories
{
    public class PhotoRepository : BaseRepository, IPhotoRepository
    {


        /// <summary>
        /// Collection de photos
        /// </summary>
        private readonly IMongoCollection<Photo> _photos;


        /// <summary>
        /// Collection d'utilisateurs
        /// </summary>
        private readonly IMongoCollection<User> _users;


        /// <summary>
        /// Dépot de photos
        /// </summary>
        public PhotoRepository() : base()
        {
            _photos = this.Database.GetCollection<Photo>("Photos");
            _users = this.Database.GetCollection<User>("Users");
        }

        public PhotoEntity Create(PhotoEntity item)
        {
            Photo photo = new Photo()
            {
                MimeType = item.MimeType,
                Content = item.Content
            };

            _photos.InsertOne(photo);

            return MapPhotoToPhotoEntity(photo);
        }


        public List<PhotoEntity> Get()
        {
            throw new NotImplementedException();
        }

        public PhotoEntity Get(string id)
        {
            Photo photo = _photos.Find(p => p.Id.ToString() == id).FirstOrDefault();

            return MapPhotoToPhotoEntity(photo);
        }

        public List<PhotoEntity> GetAllByUser(string idUser)
        {
            User user = _users.Find(u => u.Id.ToString() == idUser).FirstOrDefault();

            List<ObjectId> photos = user.Profile.Photos;

            List<Photo> found = _photos.Find(p => photos.Contains(p.Id)).ToList();

            return found.Select(p => MapPhotoToPhotoEntity(p)).ToList();    
        }

        public PhotoEntity Update(string id, PhotoEntity itemIn)
        {
            throw new NotImplementedException();
        }


        private PhotoEntity MapPhotoToPhotoEntity(Photo photo)
        {
            return new PhotoEntity
            {
                Id = photo.Id.ToString(),
                MimeType = photo.MimeType,
                Content = photo.Content
            };
        }
    }
}
