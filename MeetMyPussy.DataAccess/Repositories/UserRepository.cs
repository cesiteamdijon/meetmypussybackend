﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Repositories;
using MeetMyPussy.DataAccess.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeetMyPussy.DataAccess.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {


        /// <summary>
        /// Collection de users
        /// </summary>
        private readonly IMongoCollection<User> _users;


        /// <summary>
        /// Dépot des utilisateurs
        /// </summary>
        public UserRepository() : base()
        {
            _users = this.Database.GetCollection<User>("Users");
        }
        

        /// <summary>
        /// Obtient la liste des utilisateurs
        /// </summary>
        /// <returns></returns>
        public List<UserEntity> Get()
        {

            List<User> users = _users.Find(user => true).ToList();

            return users.Select(u => MapUserToUserEntity(u)).ToList();
        }


        /// <summary>
        /// Obtient un utilisateur via son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserEntity Get(string id)
        {
            User user = _users.Find<User>(u => u.Id == new ObjectId(id)).FirstOrDefault();

            if (user != null)
            {
                return MapUserToUserEntity(user);
            }

            return null;
        }

        /// <summary>
        /// Obtient la liste des utilisateurs dont le nom contient le nom en paramètre d'entrée
        /// </summary>
        /// <param name="name">Nom d'utilisateur recherché</param>
        /// <returns></returns>
        public List<UserEntity> GetByName(string name)
        {
            List<User> usersFound = _users.Find<User>(u => u.Profile.Name.Trim().ToLower().Contains(name.Trim().ToLower())).ToList();
            return usersFound.Select(u => MapUserToUserEntity(u)).ToList();
        }

        /// <summary>
        /// Obtient un utilisateur correspondant au login et au hash
        /// </summary>
        /// <param name="login"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public UserEntity Get(string login, string passwordHash)
        {
            User user = _users.Find<User>(u => ((u.Email == login) && (u.PasswordHash == passwordHash))).FirstOrDefault();
            if (user != null)
            {
                return MapUserToUserEntity(user);
            }
            return null;
        }


        /// <summary>
        /// Créer un utilisateur
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public UserEntity Create(UserEntity entity, string passwordHash)
        {

            User user = MapUserEnityToUser(entity);
            user.PasswordHash = passwordHash;
            
            _users.InsertOne(user);
            
            return MapUserToUserEntity(user);
        }


        /// <summary>
        /// Met à jour un utilisateur
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userIn"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public UserEntity Update(string id, UserEntity userIn, string passwordHash)
        {

            User user = MapUserEnityToUser(userIn);
            user.PasswordHash = passwordHash;

            _users.ReplaceOne(u => u.Id.ToString() == id, user);

            return Get(id);
        }


        /// <summary>
        /// Supprime un utilisateur
        /// </summary>
        /// <param name="id"></param>
        public void Remove(string id)
        {
            _users.DeleteOne(user => user.Id.ToString() == id);
        }

        /// <summary>
        /// Créer un post
        /// </summary>
        /// <param name="item"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public WallEntity CreatePost(PostEntity post, string userId)
        {
            User user = _users.FindOneAndUpdate<User>(
                u => u.Id == new ObjectId(userId),
                Builders<User>.Update.Push(u => u.Wall.Posts, MapPostEntityToPost(post)),
                new FindOneAndUpdateOptions<User, User>()
                {
                    ReturnDocument = ReturnDocument.After
                }
                );
            return MapWallToWallEntity(user.Wall);
        }

        private Post MapPostEntityToPost(PostEntity post)
        {
            return new Post
            {
                Date = post.Date,
                User = MapUserEnityToUser(post.Owner),
                Content = post.Content
            };
        }

        /// <summary>
        /// Méthode de mappage permettant de transformer un utilisateur du core en utilisateur de la repository
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private UserEntity MapUserToUserEntity(User user)
        {
            return new UserEntity
            {
                Id = user.Id.ToString(),
                Email = user.Email,
                Profile = MapProfileToProfileEntity(user.Profile),
                Wall = MapWallToWallEntity(user.Wall)
            };
        }

        private WallEntity MapWallToWallEntity(Wall wall)
        {
            return new WallEntity
            {
                Posts = wall.Posts.Select(p => MapPostToPostEntity(p)).OrderByDescending(p => p.Date).ToList()
            };
        }


        private PostEntity MapPostToPostEntity(Post post)
        {
            return new PostEntity
            {
                Date = post.Date,
                Owner = new UserEntity()
                {
                    Id = post.User.Id.ToString(),
                    Email = post.User.Email,
                    Profile = MapProfileToProfileEntity(post.User.Profile)
                },
                Content = post.Content
            };
        }
        /// <summary>
        /// Méthode de mappage permettant de transformer un profil du repository en profil du core
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        private ProfileEntity MapProfileToProfileEntity(Profile profile)
        {
            if (profile == null)
                return null;
            return new ProfileEntity
            {
                Name = profile.Name,
                City = profile.City,
                Coat = profile.Coat,
                Description = profile.Description,
                Gender = profile.Gender,
                Identification = profile.Identification,
                Race = profile.Race
            };
        }

        private Profile MapProfileEntityToProfile(ProfileEntity profileEntity)
        {
            return new Profile
            {
                Name = profileEntity.Name,
                City = profileEntity.City,
                Coat = profileEntity.Coat,
                Description = profileEntity.Description,
                Gender = profileEntity.Gender,
                Identification = profileEntity.Identification,
                Race = profileEntity.Race
            };
        }

        /// <summary>
        /// Méthode de mappage permettant de transformer un utilisateur de la repository en utilisateur du core
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private User MapUserEnityToUser(UserEntity entity)
        {
            return new User
            {
                Email = entity.Email,
                Profile = entity.Profile == null ? new Profile() : MapProfileEntityToProfile(entity.Profile),
                Wall = entity.Wall == null ? new Wall() {
                    Posts = new List<Post>()
                } : MapWallEntityToWall(entity.Wall)
            };
        }

        private Wall MapWallEntityToWall(WallEntity wallEntity)
        {
            return new Wall()
            {
                Posts = wallEntity.Posts.Select(p => MapPostEntityToPost(p)).ToList()
            };
        }

        // TODO : Réflechir sur l'interet de la repository générique
        public UserEntity Create(UserEntity item)
        {
            throw new System.NotImplementedException();
        }

        public UserEntity Update(string id, UserEntity itemIn)
        {
            throw new System.NotImplementedException();
        }
    }
}