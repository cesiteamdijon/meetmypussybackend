﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Repositories;
using MeetMyPussy.DataAccess.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeetMyPussy.DataAccess.Repositories
{
    public class ProfileRepository : BaseRepository, IProfileRepository
    {

        /// <summary>
        /// Dépot de photos
        /// </summary>
        private IPhotoRepository _photoRepository = new PhotoRepository();


        /// <summary>
        /// Collection de users
        /// </summary>
        private readonly IMongoCollection<User> _users;


        /// <summary>
        /// Collection de users
        /// </summary>
        private readonly IMongoCollection<Photo> _photos;


        /// <summary>
        /// Dépot de profils
        /// </summary>
        public ProfileRepository() : base()
        {
            _users = this.Database.GetCollection<User>("Users");
            _photos = this.Database.GetCollection<Photo>("Photos");
        }


        /// <summary>
        /// Créer un profil
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ProfileEntity AttachProfileToUser(string userId, ProfileEntity profileToAttach)
        {
            Profile profile = MapProfileEntityToProfile(profileToAttach);

            User user = _users.FindOneAndUpdate(
                u => u.Id == new ObjectId(userId),
                Builders<User>.Update.Set(u => u.Profile, profile));



            //var query = _users.Aggregate()
            //    .Match(u => u.Id == user.Id)
            //    .Unwind(u => u.Profile.Photos)
            //    .Lookup(
            //      foreignCollectionName: "Photos",
            //      localField: (User u) => u.Profile.Photos,
            //      foreignField: p => p.Id,
            //      @as: (User u) => u.Profile.Photos
            //    )
            //    .Project(u => u.)
            //    .Sort(new BsonDocument("other.name", -1))
            //    .ToList();

            //var pipeline = new[] { match };

            ProfileEntity profileUpdated = MapProfileToProfileEntity(profile);

            return profileUpdated;
        }

        public List<ProfileEntity> Get()
        {
            throw new NotImplementedException();

            //List<Profile> items = _profiles.Find(profile => true).ToList();

            //return items.Select(item => new ProfileEntity()
            //{
            //    Id = item.Id,
            //    IdUser = item.IdUser,
            //    Name = item.Name,
            //    Race = item.Race,
            //    Gender = item.Gender,
            //    Coat = item.Coat,
            //    City = item.City,
            //    Identification = item.Identification,
            //    Description = item.Description
            //}).ToList();
        }

        public ProfileEntity Get(string id)
        {
            throw new NotImplementedException();
        }

        public ProfileEntity GetByUser(string id)
        {
            User userFound = _users.Find<User>(u => u.Id == new ObjectId(id)).FirstOrDefault();

            if (userFound != null)
            {
                return MapProfileToProfileEntity(userFound.Profile);
            }
            return null;
        }

        public ProfileEntity Update(string idUser, ProfileEntity itemIn)
        {

            User user = _users.FindOneAndUpdate(
                u => u.Id == new ObjectId(idUser),
                Builders<User>.Update.Set(u => u.Profile, new Profile()
                {
                    Name = itemIn.Name,
                    Race = itemIn.Race,
                    Gender = itemIn.Gender,
                    Coat = itemIn.Coat,
                    City = itemIn.City,
                    Identification = itemIn.Identification,
                    Description = itemIn.Description
                }));


            return MapProfileToProfileEntity(user.Profile);
        }


        private Profile MapProfileEntityToProfile(ProfileEntity profileEntity)
        {
            return new Profile
            {
                Name = profileEntity.Name,
                City = profileEntity.City,
                Coat = profileEntity.Coat,
                Description = profileEntity.Description,
                Gender = profileEntity.Gender,
                Identification = profileEntity.Identification,
                Race = profileEntity.Race
            };
        }

        
        private ProfileEntity MapProfileToProfileEntity(Profile profile)
        {
            return new ProfileEntity
            {
                Name = profile.Name,
                City = profile.City,
                Coat = profile.Coat,
                Description = profile.Description,
                Gender = profile.Gender,
                Identification = profile.Identification,
                Race = profile.Race
            };
        }

        public ProfileEntity Create(ProfileEntity item)
        {
            throw new NotImplementedException();
        }
    }
}
