﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MeetMyPussy.DataAccess.Models
{
    public class Photo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        /// <summary>
        /// PROFILE | BANNER | DEFAULT
        /// </summary>
        [BsonElement("MimeType")]
        public string MimeType { get; set; }

        [BsonElement("Content")]
        public byte[] Content { get; set; }

    }
}
