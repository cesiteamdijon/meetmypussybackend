﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace MeetMyPussy.DataAccess.Models
{
    public class Profile
    {
        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Race")]
        public string Race { get; set; }

        [BsonElement("Gender")]
        public string Gender { get; set; }

        [BsonElement("Coat")]
        public string Coat { get; set; }

        [BsonElement("City")]
        public string City { get; set; }

        [BsonElement("Identification")]
        public string Identification { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("Photos")]
        public List<ObjectId> Photos { get; set; }

        [BsonIgnore]
        public Photo MainPhoto { get; set; }

        [BsonElement("MainPhoto")]
        public ObjectId MainPhotoId { get; set; }
  
        [BsonIgnore]
        public Photo BannerPhoto { get; set; }

        [BsonElement("BannerPhoto")]
        public ObjectId BannerPhotoId { get; set; }
    }
}
