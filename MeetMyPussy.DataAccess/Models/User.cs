﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MeetMyPussy.DataAccess.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("PasswordHash")]
        public string PasswordHash { get; set; }

        [BsonElement("Email")]
        public string Email { get; set; }

        [BsonElement("Profile")]
        public Profile Profile { get; set; }

        [BsonElement("Wall")]
        public Wall Wall { get; set; }

    }
}
