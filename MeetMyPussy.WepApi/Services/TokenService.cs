﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MeetMyPussy.WepApi.Services
{
    public class TokenService
    {
        
        public static string GetToken(string userId)
        {
            var claims = new[]
               {
                    new Claim("UserID", userId)
                };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("C597EBA85B6A8C597EBA85B6A8"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "10.121.128.135:5000",
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
