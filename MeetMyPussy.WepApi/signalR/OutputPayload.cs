﻿using MeetMyPussy.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeetMyPussy.WepApi.signalR
{
    public class OutputPayload
    {
        public UserEntity User { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
    }
}
