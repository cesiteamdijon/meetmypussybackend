﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Interactors.GetProfile;
using MeetMyPussy.Core.Interactors.PostProfile;
using MeetMyPussy.Core.Interactors.PutProfile;
using MeetMyPussy.DataAccess.Repositories;
using MeetMyPussy.WepApi.Input;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : AppController
    {
        #region Profile base content
        // GET: Profile
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            GetProfileByUserInteractor interactor = new GetProfileByUserInteractor(new ProfileRepository());
            var inputMessage = new GetProfileByUserInput()
            {
                User = new UserEntity()
                {
                    Id = this.DecodeToken().Id
                }
            };
            interactor.Execute(inputMessage);
            if (interactor.Output != null && interactor.Output.Profile != null)
            {
                return new JsonResult(interactor.Output.Profile);
            }
            return BadRequest("Could not get profile");
            
        }

        [HttpGet("{id}")]
        public ActionResult Get(string id)
        {
            GetProfileByUserInteractor interactor = new GetProfileByUserInteractor(new ProfileRepository());
            var inputMessage = new GetProfileByUserInput()
            {
                User = new UserEntity()
                {
                    Id = id
                }
            };
            interactor.Execute(inputMessage);
            if (interactor.Output != null && interactor.Output.Profile != null)
            {
                return new JsonResult(interactor.Output.Profile);
            }
            return BadRequest("Could not get profile");
        }

        //[HttpGet("{id}/photos")]
        //public ActionResult Get(int id)
        //{
        //    return new JsonResult(new { result = "accessing profile photos of userid: " + id.ToString() });
        //}

        [HttpPost]
        public ActionResult<IEnumerable<string>> Create([FromBody] ProfileInput input)
        {
            PostProfileInteractor interactor = new PostProfileInteractor(new ProfileRepository());
            var inputMessage = new PostProfileInput()
            {
                UserId = this.DecodeToken().Id,
                Profile = new ProfileEntity()
                {
                    Name = input.Name,
                    Race = input.Race,
                    Gender = input.Gender,
                    Coat = input.Coat,
                    City = input.City,
                    Identification = input.Identification,
                    Description = input.Description
                }
            };
            interactor.Execute(inputMessage);
            if (interactor.Output != null && interactor.Output.Profile != null)
            {
                return new JsonResult(interactor.Output.Profile);
            }

            return BadRequest("Error creating profile");
        }


        [HttpPut]
        public ActionResult<IEnumerable<string>> Edit([FromBody] ProfileInput input)
        {
            
            PutProfileInteractor interactor = new PutProfileInteractor(new ProfileRepository());

            var inputMessage = new PutProfileInput()
            {
                IdUser = this.DecodeToken().Id,
                Profile = new ProfileEntity()
                {
                    Name = input.Name,
                    Race = input.Race,
                    Gender = input.Gender, 
                    Coat = input.Coat,
                    City = input.City,
                    Identification = input.Identification,
                    Description = input.Description
                }
            };

            interactor.Execute(inputMessage);

            if (interactor.Output != null && interactor.Output.Profile != null)
            {
                return new JsonResult(interactor.Output.Profile);
            }

            return BadRequest("Error creating profile");
        }
        #endregion

        #region Profile photos

        [HttpGet("photos")]
        public ActionResult GetOwnerProfilePictures()
        {
            return new JsonResult(new { result = "accessing profile photos of owner: "  });
        }

        [HttpGet("{id}/photos")]
        public ActionResult GetAnyProfilePictures(string idProfile)
        {
            return new JsonResult(new { result = "accessing profile photos of userid: " + idProfile });
        }
        #endregion

    }
}