﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Interactors.GetUser;
using MeetMyPussy.DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : AppController
    {

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            UserEntity user = this.DecodeToken();
            if (user != null)
                return new JsonResult(user);
            return BadRequest("Could not get user");
        }

        [HttpGet("{id}")]
        public ActionResult Get(string id)
        {
            GetUserInteractor interactor = new GetUserInteractor(new UserRepository());
            var inputMessage = new GetUserInput()
            {
                Id = id

            };
            interactor.Execute(inputMessage);
            if (interactor.Output != null && interactor.Output.User != null)
            {
                return new JsonResult(interactor.Output.User);
            }
            return BadRequest("Could not get user");
        }
    }
}
