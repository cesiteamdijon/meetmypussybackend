﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Interactors;
using MeetMyPussy.Core.Interactors.PostAuth;
using MeetMyPussy.Core.Interactors.PostProfile;
using MeetMyPussy.DataAccess.Repositories;
using MeetMyPussy.Services;
using MeetMyPussy.WepApi.Input;
using MeetMyPussy.WepApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : Controller
    {

        /// <summary>
        /// Création du user puis authentification (afin de récupérer l'id user et créer le token) puis création du profil
        /// </summary>
        /// <param name=""></param>
        /// <returns>Token de l'utilisateur créé</returns>
        // POST api/values
        [HttpPost]
        public ActionResult Create([FromBody]UserWithProfileInput input)
        {

            // Création de l'utilisateur
            PostSingleUserInteractor interactorUser = new PostSingleUserInteractor(new UserRepository(), new AuthService());

            var postUserInput = new PostSingleUserInput()
            {
                Email = input.Login,
                Password = input.Password
            };

            interactorUser.Execute(postUserInput);

            if (interactorUser.Output == null || interactorUser.Output.User == null)
            {
                return BadRequest("Error creating user");
            }




            // Authentification de l'utilisateur
            PostAuthInteractor interactorAuth = new PostAuthInteractor(new UserRepository(), new AuthService());
            var authInput = new PostAuthInput()
            {
                Email = input.Login,
                Password = input.Password
            };

            interactorAuth.Execute(authInput);

            if (interactorAuth.Output == null || interactorAuth.Output.User == null)
            {
                return BadRequest("Error authentifying new user");
            }

            // Création du profil
            PostProfileInteractor interactorProfile = new PostProfileInteractor(new ProfileRepository());
            var profileInput = new PostProfileInput()
            {
                UserId = interactorUser.Output.User.Id,
                Profile = new ProfileEntity()
                {
                    Name = input.Name,
                    Race = input.Race,
                    Gender = input.Gender,
                    Coat = input.Coat,
                    City = input.City,
                    Identification = input.Identification,
                    Description = input.Description
                }
            };

            interactorProfile.Execute(profileInput);

            if (interactorProfile.Output == null || interactorProfile.Output.Profile == null)
            {
                return BadRequest("Error creating profile of new user");
            }

            return new JsonResult(new
            {
                token = TokenService.GetToken(interactorUser.Output.User.Id)
            });
        }
    }
}