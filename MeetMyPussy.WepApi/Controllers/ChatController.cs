﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetMyPussy.Core.Entities;
using MeetMyPussy.WepApi.signalR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : AppController
    {
        private IHubContext<ChatHub, ITypedHubClient> _hubContext;

        public ChatController(IHubContext<ChatHub, ITypedHubClient> hubContext)
        {
            _hubContext = hubContext;
        }

        [HttpPost]
        public string Post([FromBody]Message msg)
        {
            string retMessage = string.Empty;
            try
            {
                UserEntity user = this.DecodeToken();
                OutputPayload payload = new OutputPayload()
                {
                    Date = DateTime.Now,
                    User = user,
                    Content = msg.Payload
                };
                _hubContext.Clients.All.BroadcastMessage(msg.Type, payload);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }
            return retMessage;
        }
    }
}