﻿using MeetMyPussy.Core.Entities;
using MeetMyPussy.DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace MeetMyPussy.WepApi.Controllers
{
    [Authorize]
    public class AppController : ControllerBase
    {
        // GET: App


       
        public AppController()
        {
        }


        public UserEntity DecodeToken()
        {
            UserRepository _repo = new UserRepository();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;
                // or
                var sid = identity.FindFirst("UserID").Value;
                UserEntity UserAuthentified = _repo.Get(sid as string);
                return UserAuthentified;
            }
            return null;
        }
        

        
    }
}