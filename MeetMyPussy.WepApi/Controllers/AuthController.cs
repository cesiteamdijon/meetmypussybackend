﻿using MeetMyPussy.Core.Interactors.PostAuth;
using MeetMyPussy.DataAccess.Repositories;
using MeetMyPussy.Services;
using MeetMyPussy.WepApi.Input;
using MeetMyPussy.WepApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        [HttpPost]       
        public ActionResult Get([FromBody]CredentialsInput input)
        {
            return Authentificate(input.Login, input.Password);
        }

        public ActionResult Authentificate(string login, string password)
        {

            PostAuthInteractor interactor = new PostAuthInteractor(new UserRepository(), new AuthService());
            var inputMessage = new PostAuthInput()
            {
                Email = login,
                Password = password
            };
            interactor.Execute(inputMessage);

            if (interactor.Output != null && interactor.Output.User != null)
            {
                return Ok(new
                {
                    token = TokenService.GetToken(interactor.Output.User.Id)
                });
            }

            return BadRequest("Could not verify username and password");
        }
    }


}