﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetMyPussy.Core.Entities;
using MeetMyPussy.Core.Interactors.PostPost;
using MeetMyPussy.DataAccess.Repositories;
using MeetMyPussy.WepApi.Input;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : AppController
    {
        [HttpPost]
        public ActionResult Post([FromBody]PostInput input)
        {
            PostPostInteractor interactor = new PostPostInteractor(new UserRepository());
            UserEntity currentUser = this.DecodeToken();
            PostPostInput postInput = new PostPostInput()
            {
                User = new UserEntity()
                {
                    Id = currentUser.Id,
                    Email = currentUser.Email,
                    Profile = new ProfileEntity()
                    {
                        Name = currentUser.Profile.Name
                    }
                },
                WallOwnerId = input.IdOwner,
                Content = input.Content

            };
            interactor.Execute(postInput);
            if (interactor.Output != null && interactor.Output.Wall != null)
            {
                return new JsonResult(interactor.Output.Wall);
            }
            return BadRequest("Impossible de créer un post");
        }
    }
}
