﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetMyPussy.Core.Interactors.GetProfilesByName;
using MeetMyPussy.DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MeetMyPussy.WepApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : AppController
    {
        [HttpGet("users/{name}")]
        public ActionResult<IEnumerable<string>> Get(string name)
        {
            GetProfilesByNameInteractor interactor = new GetProfilesByNameInteractor(new UserRepository());

            var inputMessage = new GetProfilesByNameInput()
            {
                Name = name
            };
            interactor.Execute(inputMessage);
            // On ne vérifie pas si interactor.Output.Users est null car c'est considéré comme un comportement normal
            // (la recherche n'a rien trouvé en base)
            if (interactor.Output != null)
            {
                return new JsonResult(interactor.Output);
            }
            return BadRequest("Could not get profiles");

        }
    }
}
