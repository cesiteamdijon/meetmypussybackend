﻿namespace MeetMyPussy.WepApi.Input
{
    public class ProfileInput
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Race { get; set; }
        public string Gender { get; set; }
        public string Coat { get; set; }
        public string City { get; set; }
        public string Identification { get; set; }
        public string Description { get; set; }
    }
}
