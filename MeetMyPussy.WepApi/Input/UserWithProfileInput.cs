﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeetMyPussy.WepApi.Input
{
    public class UserWithProfileInput
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Race { get; set; }
        public string Gender { get; set; }
        public string Coat { get; set; }
        public string City { get; set; }
        public string Identification { get; set; }
        public string Description { get; set; }
        public IFormFile MainPhoto { get; set; }
        public IFormFile BannerPhoto { get; set; }
    }
}
